package be.greifmatthias.framed.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.greifmatthias.framed.AppListHeader;
import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.R;

public class AppListAdapter extends BaseAdapter {

    // Installed apps
    private List<Object> apps;

    private LayoutInflater inflater;

    // Types in applist, App or Header
    private static final int TYPE_APP = 0;
    private static final int TYPE_HEADER = 1;

    // Default constructor
    public AppListAdapter(Context context, ArrayList<AppModel> apps){
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.apps = new ArrayList<Object>();

        // Convert apps to model and add headers
        String lastheader = "";
        for (AppModel entry:apps) {
            // Check if app has same first letter, then in same header else in insert new header
            if(!entry.getName().substring(0, 1).equals(lastheader)){
                lastheader = entry.getName().substring(0, 1);
                this.apps.add(new AppListHeader(lastheader));
            }

            // Add app
            this.apps.add(entry);
        }

    }

    // Return size of list
    @Override
    public int getCount() {
        return apps.size();
    }

    // Get app or header item from list
    @Override
    public Object getItem(int position) {
        return apps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Get number of different types in list
    @Override
    public int getViewTypeCount() {
        return 2;
    }

    // Return type of element in list
    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof AppModel) {
            return TYPE_APP;
        }

        return TYPE_HEADER;
    }

    // Get view of element in position of list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get type of item
        int type = this.getItemViewType(position);

        // Generate new view if not passed
        if (convertView == null) {
            switch (type) {
                case TYPE_APP:
                    convertView = this.inflater.inflate(R.layout.applist_appentry_tile, parent, false);
                    break;
                case TYPE_HEADER:
                    convertView = this.inflater.inflate(R.layout.applist_appentry_header, parent, false);
                    break;
            }
        }

        // Set data of view
        switch (type) {
            case TYPE_APP:
                // Get element in list
                AppModel app = (AppModel) getItem(position);

                // Get controls
                TextView name = (TextView)convertView.findViewById(R.id.tvAppname);
                ImageView icon = (ImageView) convertView.findViewById(R.id.ivIcon);
                LinearLayout llIcon = (LinearLayout)convertView.findViewById(R.id.llIcon);

                // Set UI
                llIcon.setBackgroundColor(app.getColor());

//                if(app.isGame()){
//                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(icon.getLayoutParams());
//                    lp.setMargins(0,0,0,0);
//                    icon.setLayoutParams(lp);
//                }

                // Set values
                name.setText(app.getName()); // + " isgame " + app.isGame() + " hasicon " + app.isCustomIconSupported()
                icon.setImageDrawable(app.getIcon());

                break;
            case TYPE_HEADER:
                // Get element in list
                AppListHeader header = (AppListHeader) getItem(position);

                // Get controls
                TextView title = (TextView)convertView.findViewById(R.id.textSeparator);

                // Set values
                title.setText(header.getTitle());

                break;
        }

        return convertView;
    }
}