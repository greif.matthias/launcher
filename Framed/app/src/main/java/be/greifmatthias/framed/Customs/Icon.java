package be.greifmatthias.framed.Customs;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class Icon {
    // PUBLIC
    // Check if icon resource for application is available
    public static boolean isSupported(Context context, String packagename){
        return Icon.getResourceId(context, packagename) != 0;
    }

    // Get drawaable resource of icon for application, returns null if not available
    public static Drawable getIcon(Context context, String packagename){
        int rId = Icon.getResourceId(context, packagename);

        if(rId != 0){
            Resources r = context.getResources();
            return r.getDrawable(rId);
        }

        return null;
    }

    //PRIVATE
    private static int getResourceId(Context context, String packagename){
        return context.getResources().getIdentifier("x_" + packagename.toLowerCase().replace('.', '_'), "drawable", context.getPackageName());
    }
}
