package be.greifmatthias.framed.DialogViews;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;

import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.MainActivity;
import be.greifmatthias.framed.R;
import be.greifmatthias.framed.Systems.Application;

public class AppEntryListAppDialogView extends DialogView {
    public void show(final Activity activity, final AppModel app){
        // Setup dialog
        final Dialog dialog = new Dialog(activity, R.style.FullscreenDialog);
        dialog.setContentView(R.layout.dialogview_appentrylistapp);

        // Setup UI
        LinearLayout llRoot = (LinearLayout)dialog.findViewById(R.id.llRoot);
        llRoot.setBackgroundColor(app.getColor());

        // Get controls
        TextView tvAppname = (TextView) dialog.findViewById(R.id.tvAppname);
        TextView tvPackagename = (TextView)dialog.findViewById(R.id.tvPackagename);
        ImageView ivIcon = (ImageView)dialog.findViewById(R.id.ivIcon);
        Button btnPintostart = (Button) dialog.findViewById(R.id.btnPintostart);
        Button btnInformation = (Button)dialog.findViewById(R.id.btnAppinfo);
        Button btnUninstall = (Button)dialog.findViewById(R.id.btnUninstall);

        // Set vals
        tvAppname.setText(app.getName());
        tvPackagename.setText(app.getPackagename());
        ivIcon.setImageDrawable(app.getIcon());

        // Hide uninstall button if system app
        if(app.isSystemApplication()){
            btnUninstall.setVisibility(View.GONE);
        }

        // Clicks
        RelativeLayout rlDialog = (RelativeLayout) dialog.findViewById(R.id.rlDialog);
        rlDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnPintostart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((MainActivity)activity).pinToHome(app);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });

        btnInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Application.requestInformationscreen(activity, app.getPackagename());
            }
        });

        btnUninstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Application.requestDelete(activity, app.getPackagename());
            }
        });

        // Show
        dialog.show();
    }
}
