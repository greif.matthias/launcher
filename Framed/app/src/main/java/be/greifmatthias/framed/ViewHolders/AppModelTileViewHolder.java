package be.greifmatthias.framed.ViewHolders;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import be.greifmatthias.framed.Adapters.DragGridAdapter;
import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.R;

public class AppModelTileViewHolder extends RecyclerView.ViewHolder {


    public AppModel item;

    public AppModelTileViewHolder(View itemView, DragGridAdapter.OnItemClickListener listener) {
        super(itemView);
    }

    public void bindData(final AppModel model, final DragGridAdapter.OnItemClickListener listener) {
        this.item = model;

        item.bindViewHolder(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(item);
            }
        });
    }
}