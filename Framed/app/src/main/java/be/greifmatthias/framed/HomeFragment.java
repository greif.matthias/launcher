package be.greifmatthias.framed;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.arasthel.spannedgridlayoutmanager.SpannedGridLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import be.greifmatthias.framed.Adapters.DragGridAdapter;
import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.Systems.StatusBar;

import static be.greifmatthias.framed.MainActivity.KEY_HOMELAYOUT;
import static be.greifmatthias.framed.MainActivity.PREF_APP;

public class HomeFragment extends Fragment {
    private RecyclerView rvTiles;
    private DragGridAdapter adapter;

    private ArrayList<AppModel> apps;

    public DragGridAdapter getAdapter() {
        return this.adapter;
    }

    public HomeFragment() { }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Get view
        final View view = inflater.inflate(R.layout.home_fragment, container, false);

        // Set adapter for list
        this.rvTiles = (RecyclerView)view.findViewById(R.id.rvTiles);
        this.adapter = new DragGridAdapter(this.apps, new DragGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(AppModel item) {
                item.launch(getActivity());
            }
        });
        this.rvTiles.setAdapter(this.adapter);

        // Set layout for grid
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        this.rvTiles.setLayoutManager(layoutManager);

//        SpannedGridLayoutManager spannedGridLayoutManager = new SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, 3);
//        this.rvTiles.setLayoutManager(spannedGridLayoutManager);


        ItemTouchHelper.Callback callback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                return makeMovementFlags(dragFlags, 0);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                adapter.onItemMove(viewHolder.getLayoutPosition(), target.getLayoutPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }
        };
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(this.rvTiles);

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)view.findViewById(R.id.llHome).getLayoutParams();
        lp.setMargins(0, StatusBar.getHeight(getActivity()), 0, 0);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Load homelayout
        this.load();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void addTile(AppModel app) throws JSONException {
        this.adapter.addItem(app);
        this.apps.add(app);

        this.save();
    }

    private void save() throws JSONException {
        //Save layout
        SharedPreferences sp = getActivity().getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_HOMELAYOUT, this.layoutToJSON());
        editor.commit();
    }

    private void load(){
        //Load layout
        SharedPreferences sp = getActivity().getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        String layout = sp.getString(KEY_HOMELAYOUT, "");

        try {
            this.JSONToLayout(layout);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Convert current layout to json string
    private String layoutToJSON() throws JSONException {
        // Init
        JSONArray layout = new JSONArray();

        // Convert objects to json string
        for (AppModel app : this.apps) {
            layout.put(app.toJSON());
        }

        // Assemble json string
        JSONObject output = new JSONObject();
        output.put("layout", layout);

        return output.toString();
    }

    // Load up a json layout string to layout
    private void JSONToLayout(String json) throws JSONException, PackageManager.NameNotFoundException {
        // Init
        this.apps = new ArrayList<AppModel>();

        // Load json layout if possible
        if(json != ""){
            // Convert json string
            JSONObject object = new JSONObject(json);

            // Add apps to homelayout list
            for (int i = 0; i < object.getJSONArray("layout").length(); i++) {
                JSONObject element = new JSONObject(object.getJSONArray("layout").getString(i));

                this.apps.add(AppModel.load(element, getActivity()));
            }
        }
    }
}
