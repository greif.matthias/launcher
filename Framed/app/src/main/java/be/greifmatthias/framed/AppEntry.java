package be.greifmatthias.framed;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

public class AppEntry {
    private String name;
    private String packagename;
    private Drawable icon;
    private Boolean issystemapplication;
    private Boolean isgame;

    public AppEntry(){}

    public AppEntry(Context context, CharSequence name, String packagename, Drawable icon) throws PackageManager.NameNotFoundException {
        this.name = name.toString();
        this.packagename = packagename;

        this.icon = icon;

        be.greifmatthias.framed.Systems.Application appclass = new be.greifmatthias.framed.Systems.Application(context);
        this.issystemapplication = appclass.isSystemApplication(packagename);
        this.isgame = appclass.isGame(packagename);
    }

    public AppEntry(Context context, String packagename) throws PackageManager.NameNotFoundException {
        //Get appinfo by packagename
        be.greifmatthias.framed.Systems.Application appclass = new be.greifmatthias.framed.Systems.Application(context);
        AppEntry lookup = appclass.getByPackagename(packagename);

        this.name = lookup.getName();
        this.packagename = lookup.getPackagename();

        this.icon = lookup.getIcon();

        this.issystemapplication = appclass.isSystemApplication(packagename);
        this.isgame = appclass.isGame(packagename);
    }

    public String getName(){
        return this.name;
    }

    public String getPackagename(){
        return this.packagename;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public Boolean isSystemApplication() {
        return this.issystemapplication;
    }

    public Boolean isGame() {
        return this.isgame;
    }

    protected void setIcon(Drawable icon){
        this.icon = icon;
    }

    public void launch(Context context){
        Intent i = context.getPackageManager().getLaunchIntentForPackage(this.getPackagename());
        context.startActivity(i);
    }
}
