package be.greifmatthias.framed.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import be.greifmatthias.framed.AppsFragment;
import be.greifmatthias.framed.HomeFragment;

public class MainscreenAdapter extends FragmentPagerAdapter {
    // All available pages
    private ArrayList<Fragment> pages;
    // Current pageindex
    private int currentpage;

    public MainscreenAdapter(FragmentManager fm) {
        super(fm);

        // Load up pages
        this.pages = new ArrayList<Fragment>();

        this.pages.add(new HomeFragment());
        this.pages.add(new AppsFragment());
    }

    // Get page for requested position
    @Override
    public Fragment getItem(int position) {
        // Correct get request to Homepage if needed
        if(position > this.pages.size() || position < 0){
            position = 0;
        }

        // Update position val
        this.currentpage = position;

        // Return requested page
        return this.pages.get(position);
    }

    // Get number of pages
    @Override
    public int getCount() {
        return this.pages.size();
    }

    // Return position of current page
    public int getCurrentpage(){
        return this.currentpage;
    }
}