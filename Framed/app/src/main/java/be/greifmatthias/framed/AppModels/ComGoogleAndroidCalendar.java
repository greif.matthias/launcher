package be.greifmatthias.framed.AppModels;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import be.greifmatthias.framed.AppEntry;
import be.greifmatthias.framed.R;
import be.greifmatthias.framed.Settings;

public class ComGoogleAndroidCalendar extends AppModel {
    private Context _context;

    private String _date;
    private String _day;

    private String[] _days = {
            "MON",
            "TUE",
            "WEN",
            "THU",
            "FRI",
            "SAT",
            "SUN"
    };

    public ComGoogleAndroidCalendar(Context context, AppEntry entry, boolean hide) throws PackageManager.NameNotFoundException {
        super(context, entry, hide);

        this._context = context;

        this._date = "" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) ;
        this._day = this._days[Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 2];
    }

    @Override
    public int getTileView() {
        // Return layout
        switch (this.getSize()){
            case SMALL:
                return R.layout.homefragment_appmodel_tile_small_calendar;
            default:
                return R.layout.homefragment_appmodel_tile_small_calendar;
        }
    }

    @Override
    public void bindViewHolder(View itemView){
        TextView tvDate;
        TextView tvDay;
        ImageView ivIcon;
        RelativeLayout rlRoot;

        if(itemView.findViewById(R.id.tvDate) != null){
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvDate.setText(this._date);
        }

        if(itemView.findViewById(R.id.tvDay) != null){
            tvDay = (TextView) itemView.findViewById(R.id.tvDay);
            tvDay.setText(this._day);
        }

        if(itemView.findViewById(R.id.ivIcon) != null){
            ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);

            // Set tile icon
            Resources r = this._context.getResources();
            ivIcon.setImageDrawable(r.getDrawable(R.drawable.x_com_google_android_calendar_dark));
        }
    }
}
