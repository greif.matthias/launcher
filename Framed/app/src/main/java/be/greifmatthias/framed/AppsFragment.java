package be.greifmatthias.framed;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

import be.greifmatthias.framed.Adapters.AppListAdapter;
import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.DialogViews.AppEntryListAppDialogView;
import be.greifmatthias.framed.DialogViews.AppEntryListLetterNavDialogView;
import be.greifmatthias.framed.Listeners.AppsFragmentPackageAddedListener;
import be.greifmatthias.framed.Listeners.AppsFragmentPackageRemovedListener;

public class AppsFragment extends Fragment {
    private PackageManager _manager;
    private AppEntryListLetterNavDialogView appEntryListLetterNavDialogView;

    public AppsFragment() {}

    @SuppressWarnings("unused")
    public static AppsFragment newInstance() {
        return new AppsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Get view
        View view = inflater.inflate(R.layout.applist_fragment, container, false);

        // Get packagemanager
        this._manager = PackageManager.getInstance(getContext());

        // Add listener for packagechanges
        AppsFragmentPackageAddedListener addedListener = new AppsFragmentPackageAddedListener();
        this._manager.addPackageaddedlistener(addedListener);
        AppsFragmentPackageRemovedListener removedListener = new AppsFragmentPackageRemovedListener();
        this._manager.addPackageremovedlistener(removedListener);

        // Init dialog
        this.appEntryListLetterNavDialogView = AppEntryListLetterNavDialogView.getInstance(getContext(), (MainActivity)getActivity());

        // Set ui
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)view.findViewById(R.id.llHeaderContent).getLayoutParams();
        lp.setMargins(0, be.greifmatthias.framed.Systems.StatusBar.getHeight(getActivity()), 0, 0);

        // Set the adapter for list
        ListView lvApps = (ListView) view.findViewById(R.id.lvApps);
        lvApps.setAdapter(new AppListAdapter(view.getContext(), new ArrayList<AppModel>(this._manager.getApplications())));
        registerForContextMenu(lvApps);

        // When clicking on an item in list
        lvApps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // if app
                if(parent.getItemAtPosition(position) instanceof AppModel){
                    // Get element of list
                    AppModel app = (AppModel) parent.getItemAtPosition(position);

                    // Start requested app
                    app.launch(getActivity());

                    // Autonav to homepage
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    ((MainActivity)getActivity()).scrollToHome();
                                }
                            },750);
                }

                // if header
                if(parent.getItemAtPosition(position) instanceof AppListHeader){
                    // Get headerinformation
                    //AppListHeader header = (AppListHeader) parent.getItemAtPosition(position);

                    // Show header selection screen
                    appEntryListLetterNavDialogView.show();
                }
            }
        });

        // When longpressing item in list
        lvApps.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Check if app
                if(parent.getItemAtPosition(position) instanceof AppModel) {
                    // Get app information
                    AppModel entry = (AppModel) parent.getItemAtPosition(position);

                    // Show dialog
                    new AppEntryListAppDialogView().show(getActivity(), entry);
                }

                return true;
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
