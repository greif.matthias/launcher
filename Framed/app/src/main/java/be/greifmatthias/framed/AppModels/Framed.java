package be.greifmatthias.framed.AppModels;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import be.greifmatthias.framed.AppEntry;
import be.greifmatthias.framed.R;
import be.greifmatthias.framed.Settings;

public class Framed extends AppModel {
    public Framed(Context context, AppEntry entry, boolean hide) throws PackageManager.NameNotFoundException {
        super(context, entry, hide);
    }

    @Override
    public void launch(Context context){
        Intent intent = new Intent(context, Settings.class);
        context.startActivity(intent);
    }
}
