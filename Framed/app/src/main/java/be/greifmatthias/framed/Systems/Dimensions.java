package be.greifmatthias.framed.Systems;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Dimensions {
    public static float pixelsToDp(float pixel){
        DisplayMetrics m = Resources.getSystem().getDisplayMetrics();
        return Math.round(pixel / (m.densityDpi / 160f));
    }

    public static float dpToPixels(float dp){
        DisplayMetrics m = Resources.getSystem().getDisplayMetrics();
        return Math.round(dp * (m.densityDpi / 160f));
    }
}
