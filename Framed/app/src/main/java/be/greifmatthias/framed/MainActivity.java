package be.greifmatthias.framed;

import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;

import android.widget.FrameLayout;

import org.json.JSONException;

import be.greifmatthias.framed.Adapters.MainscreenAdapter;
import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.Systems.StatusBar;

public class MainActivity extends AppCompatActivity
{
    // Navigation adapter for mainactivity
    private MainscreenAdapter adapter;
    // Navigator content controller
    private ViewPager vpScreens;

    private PackageManager _manager;

    // SavedPreference Key values
    public static final String PREF_APP = "PREF_APP";
    public static final String KEY_HOMELAYOUT = "KEY_HOMELAYOUT";

    private final String TAG = "be.greifmatthias.framed.MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // Setup packagemanager
        this._manager = PackageManager.getInstance(getApplicationContext());

        //Setup UI
        //Draw behind statusbar
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams)this.findViewById(R.id.main_content).getLayoutParams();
        lp.setMargins(0, -StatusBar.getHeight(getApplicationContext()), 0, 0);

        // Setup adapter and control
        // Horizontal scroll through home
        this.adapter = new MainscreenAdapter(getSupportFragmentManager());
        this.vpScreens = (ViewPager) findViewById(R.id.vpScreens);
        this.vpScreens.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        this.scrollToHome();
        return;
    }

    // Go to homepage
    public void scrollToHome(){
        if(this.adapter.getCurrentpage() > 0){
            this.vpScreens.setCurrentItem(0, true);
        }
    }

    public void pinToHome(AppModel app) throws JSONException {
        // Get adapter of homepage to add item
        ((HomeFragment)this.adapter.getItem(0)).addTile(app);
    }
}
