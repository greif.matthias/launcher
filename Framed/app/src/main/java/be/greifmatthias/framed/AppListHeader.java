package be.greifmatthias.framed;

public class AppListHeader {
    private String title;

    public AppListHeader(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }
}
