package be.greifmatthias.framed.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class PackageReceiver extends BroadcastReceiver {

    private final String TAG = "be.greifmatthias.framed.PackageReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        // Check if launched by replace
        boolean replacing = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false);

        if(!replacing) {
            Intent sendintent = new Intent(this.TAG);
            sendintent.putExtra("ACTION", intent.getAction());
            sendintent.putExtra("PACKAGE", intent.getData().toString());

            // Sent local broadcast
            LocalBroadcastManager.getInstance(context).sendBroadcast(sendintent);
        }
    }
}