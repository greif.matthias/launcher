package be.greifmatthias.framed.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.R;

public class AppListLetterAdapter extends BaseAdapter {
    private ArrayList<String> letters;
    private String[] allLetters;
    private LayoutInflater inflater;

    private static final int TYPE_APP = 0;
    private static final int TYPE_HEADER = 1;

    public AppListLetterAdapter(Context context, ArrayList<AppModel> apps){
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Load letters
        String lastheader = "";
        this.letters = new ArrayList<String>();
        for (AppModel entry:apps) {
            if(!entry.getName().substring(0, 1).equals(lastheader)){
                lastheader = entry.getName().substring(0, 1);
                this.letters.add(lastheader);
            }
        }

        // Set alphabet
        this.allLetters = new String[]{
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z"
        };
    }

    @Override
    public int getCount() {
        return this.allLetters.length;
    }

    @Override
    public Object getItem(int position) {
        return this.allLetters[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        String letter = (String)getItem(position);

        return this.letters.contains(letter);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.dialogview_appentryletternav_letterentry_tile, parent, false);

        String letter = (String)getItem(position);

        // Set content
        TextView tvLetter = (TextView)convertView.findViewById(R.id.tvLetter);
        tvLetter.setText(letter);

        // Setup UI
        if(!this.isEnabled(position)){
            convertView.setBackgroundColor(Color.parseColor("#EEEEEE"));
        }

        return convertView;
    }
}
