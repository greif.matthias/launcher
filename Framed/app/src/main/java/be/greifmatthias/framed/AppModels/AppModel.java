package be.greifmatthias.framed.AppModels;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import be.greifmatthias.framed.AppEntry;
import be.greifmatthias.framed.Customs.Icon;
import be.greifmatthias.framed.R;

public class AppModel extends AppEntry {
    private boolean hidden;
    private int color;
    private boolean iscustomiconsupported;

    private TILE_SIZES _size;

    public enum TILE_SIZES { SMALL, NORMAL, WIDE, LARGE }

    public AppModel(Context context, AppEntry entry, boolean hide) throws PackageManager.NameNotFoundException {
        // Default constructor
        super(context, entry.getName(), entry.getPackagename(), entry.getIcon());

        this.hidden = hide;

//        this.color = be.greifmatthias.framed.System.Colors.findClosestToPalette(be.greifmatthias.framed.System.Colors.getDominantColor(be.greifmatthias.framed.System.Imaging.drawableToBitmap((this.getIcon()))),
//                be.greifmatthias.framed.System.Colors.getMaterial());

        this.color = be.greifmatthias.framed.Systems.Color.getDominantColor(be.greifmatthias.framed.Systems.Imaging.drawableToBitmap((this.getIcon())));

        // Try to set custom icon for app
        this.iscustomiconsupported = false;
        Drawable i = new Icon().getIcon(context, this.getPackagename());
        if(i != null){
            this.setIcon(i);
            this.iscustomiconsupported = true;
        }

        if(this.isSystemApplication()){
            this.color = Color.parseColor("#FF2196F3");
        }

        this._size = TILE_SIZES.NORMAL;
    }

    public Boolean isHidden(){
        return this.hidden;
    }

    public void hide(boolean hide){
        this.hidden = hide;
    }

    public int getColor() { return this.color; }

    public boolean isCustomIconSupported() {
        return this.iscustomiconsupported;
    }

    public int getTileView(){
        switch (this._size){
            case SMALL:
                return R.layout.homefragment_appmodel_tile_small;
            default:
                return R.layout.homefragment_appmodel_tile_small;
        }
    }

    public String toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("packagename", this.getPackagename());
        json.put("size", sizetoint(this.getSize()));
        json.put("hidden", this.isHidden());

        return json.toString();
    }

    public static AppModel getByEntry(AppEntry entry, Context context) throws PackageManager.NameNotFoundException {
            switch (entry.getPackagename()){
                case "be.greifmatthias.framed":
                    return new Framed(context, entry, false);
                case "com.google.android.calendar":
                    return new ComGoogleAndroidCalendar(context, entry, false);
                default:
                    return new AppModel(context, entry, false);
            }
    }

    public static AppModel load(JSONObject json, Context context) throws JSONException, PackageManager.NameNotFoundException {
        // Get entry
        AppEntry entry = new AppEntry(context, json.getString("packagename"));
        AppModel model = getByEntry(entry, context);

        model.hidden = json.getBoolean("hidden");
        model._size = inttosize(json.getInt("size"));

        // Load up color
        model.color = be.greifmatthias.framed.Systems.Color.getDominantColor(be.greifmatthias.framed.Systems.Imaging.drawableToBitmap((entry.getIcon())));

        // Load icon
        model.iscustomiconsupported = false;
        Drawable i = new Icon().getIcon(context, model.getPackagename());
        if(i != null){
            model.setIcon(i);
            model.iscustomiconsupported = true;
        }

        // Check if system application
        if(model.isSystemApplication()){
            model.color = Color.parseColor("#FF2196F3");
        }
        return model;
    }

    public TILE_SIZES getSize() {
        return this._size;
    }

    private static int sizetoint(TILE_SIZES size){
        switch (size){
            case SMALL:
                return 0;
            case WIDE:
                return 2;
            case LARGE:
                return 3;
            default:
                return 1;
        }
    }

    private static TILE_SIZES inttosize(int size){
        switch (size){
            case 0:
                return TILE_SIZES.SMALL;
            case 2:
                return TILE_SIZES.WIDE;
            case 3:
                return TILE_SIZES.LARGE;
            default:
                return TILE_SIZES.NORMAL;
        }
    }

    public void bindViewHolder(View itemView){
        TextView appname;
        ImageView icon;
        RelativeLayout rlRoot;

        if(itemView.findViewById(R.id.tvAppname) != null){
            appname = (TextView) itemView.findViewById(R.id.tvAppname);
            appname.setText(this.getName());
        }

        if(itemView.findViewById(R.id.ivIcon) != null){
            icon = (ImageView) itemView.findViewById(R.id.ivIcon);
            icon.setImageDrawable(this.getIcon());
        }

        rlRoot = (RelativeLayout)itemView.findViewById(R.id.rlRoot);
        rlRoot.setBackgroundColor(this.getColor());
    }
}