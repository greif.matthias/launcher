package be.greifmatthias.framed.DialogViews;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.GridView;

import java.util.ArrayList;

import be.greifmatthias.framed.Adapters.AppListLetterAdapter;
import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.MainActivity;
import be.greifmatthias.framed.PackageManager;
import be.greifmatthias.framed.R;

public class AppEntryListLetterNavDialogView {
    private PackageManager _manager;

    private static AppEntryListLetterNavDialogView view;

    private Dialog dialog;

    public AppEntryListLetterNavDialogView(Context context, Activity root){
        this._manager = PackageManager.getInstance(context);

        // Setup dialog
        dialog = new Dialog(root, R.style.FullscreenDialog);
        dialog.setContentView(R.layout.dialogview_appentryletternav);

        // Setup UI
        GridView gvLetters = (GridView) dialog.findViewById(R.id.gvLetters);
        gvLetters.setAdapter(new AppListLetterAdapter(context, new ArrayList<AppModel>(this._manager.getApplications())));
    }

    public static AppEntryListLetterNavDialogView getInstance(Context context, Activity root){
        if(view == null){
            view = new AppEntryListLetterNavDialogView(context, root);
        }

        return view;
    }

    public void show(){
        // Show
        dialog.show();
    }
}
