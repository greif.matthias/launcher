package be.greifmatthias.framed.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arasthel.spannedgridlayoutmanager.SpanLayoutParams;
import com.arasthel.spannedgridlayoutmanager.SpanSize;

import java.util.ArrayList;
import java.util.Collections;

import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.R;
import be.greifmatthias.framed.ViewHolders.AppModelTileViewHolder;

public class DragGridAdapter extends RecyclerView.Adapter<AppModelTileViewHolder> implements ItemTouchHelperAdapter {
    private ArrayList<AppModel> items;
    private final OnItemClickListener listener;

    public DragGridAdapter(ArrayList<AppModel> items, OnItemClickListener listener) {
        this.listener = listener;

        this.items = new ArrayList<AppModel>();
        for (AppModel entry: items) {
            this.items.add(entry);
        }
    }

    @NonNull
    @Override
    public AppModelTileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        AppModelTileViewHolder holder =  new AppModelTileViewHolder(view, this.listener);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AppModelTileViewHolder holder, int position) {
        ((AppModelTileViewHolder)holder).bindData((AppModel)this.items.get(position), this.listener);

//        holder.itemView.setLayoutParams(new SpanLayoutParams(new SpanSize(((AppModel)this.items.get(position)).getSpanwidth(), ((AppModel)this.items.get(position)).getSpanheight())));
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(final int position) {
        return getItem(position).getTileView();
    }

    public ArrayList<AppModel> getItems() {
        return items;
    }

    public AppModel getItem(int position){ return this.items.get(position); }

    public interface OnItemClickListener {
        void onItemClick(AppModel item);
    }

    // On adding tile
    public void addItem(AppModel app){
        this.items.add(app);
        this.notifyItemInserted(this.getItemCount() - 1);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(this.items, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(this.items, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }
}

