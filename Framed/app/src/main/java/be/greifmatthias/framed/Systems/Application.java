package be.greifmatthias.framed.Systems;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.Settings;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import be.greifmatthias.framed.AppEntry;

public class Application {
    private Context context;
    private PackageManager packageManager;

    public Application(Context context){
        this.context = context;
        this.packageManager = context.getPackageManager();
    }

    public ArrayList<AppEntry> getAll(){
        // Init
        ArrayList<AppEntry> apps = new ArrayList<AppEntry>();

        // Load apps
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = this.packageManager.queryIntentActivities(i, 0);
        for (ResolveInfo ri : availableActivities) {
            AppEntry app = null;
            try {
                app = new Application(this.context).getByPackagename(ri.activityInfo.packageName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            apps.add(app);
        }

        return apps;
    }

    public AppEntry getByPackagename(String packagename) throws PackageManager.NameNotFoundException {
        ApplicationInfo app = this.packageManager.getApplicationInfo(packagename, 0);

        return new AppEntry(context, this.packageManager.getApplicationLabel(app), packagename, this.packageManager.getApplicationIcon(app));
    }


    public Boolean isSystemApplication(String packagename) throws PackageManager.NameNotFoundException {
        ApplicationInfo ai = packageManager.getApplicationInfo(packagename, 0);

        if ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
            return true;
        }

        return false;
    }

    public Boolean isGame(String packagename) throws PackageManager.NameNotFoundException {
        ApplicationInfo ai = this.packageManager.getApplicationInfo(packagename, 0);

        if ((ai.flags & ApplicationInfo.FLAG_IS_GAME) == ApplicationInfo.FLAG_IS_GAME) {
            return true;
        }

        return false;
    }

    public static void requestDelete(Context context, String packagename){
        Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:" + packagename));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void requestInformationscreen(Context context, String packagename){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", packagename, null);
        intent.setData(uri);
        context.startActivity(intent);
    }
}
