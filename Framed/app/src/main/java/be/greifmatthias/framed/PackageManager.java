package be.greifmatthias.framed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import be.greifmatthias.framed.AppModels.AppModel;
import be.greifmatthias.framed.AppModels.Framed;
import be.greifmatthias.framed.Listeners.PackageAddedListener;
import be.greifmatthias.framed.Listeners.PackageRemovedListener;

public class PackageManager {
    private static PackageManager _manager;

    private Context _context;
    private List<AppModel> _applications;
    private List<PackageAddedListener> _addedlisteners;
    private List<PackageRemovedListener> _removedlisteners;

    public static PackageManager getInstance(Context context) {
        if(_manager == null){
            _manager = new PackageManager(context);
        }

        return _manager;
    }

    public PackageManager(Context context){
        // Set vars
        this._context = context;
        this._applications = new ArrayList<>();
        this._addedlisteners = new ArrayList<>();
        this._removedlisteners = new ArrayList<>();

        // Load application list
        List<AppEntry> appEntries = this.loadApplications();

        // Revert list to appmodels
        for (AppEntry entry : appEntries) {
            try {
                this._applications.add(AppModel.getByEntry(entry, _context));
            } catch (android.content.pm.PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        // Sort app list on name
        Collections.sort(this._applications, new Comparator<AppModel>() {
            @Override
            public int compare(AppModel o1, AppModel o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        // Setup local broadcastmanager for package changes
        LocalBroadcastManager.getInstance(this._context).registerReceiver(this._packagereceiver, new IntentFilter("be.greifmatthias.framed.PackageReceiver"));
    }

    // Receiver on package changes detected
    private BroadcastReceiver _packagereceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Uri data = intent.getData();

            // Update listeners
            if(intent.getStringExtra("ACTION").equals("android.intent.action.PACKAGE_ADDED")){
                for(PackageAddedListener l : _addedlisteners){
                    l.update(intent.getStringExtra("PACKAGE"));
                }
            }else{
                for(PackageRemovedListener l : _removedlisteners){
                    l.update(intent.getStringExtra("PACKAGE"));
                }
            }
        }
    };

    private List<AppEntry> loadApplications(){
        return new be.greifmatthias.framed.Systems.Application(this._context).getAll();
    }

    public List<AppModel> getApplications(){
        return this._applications;
    }

    public void addPackageaddedlistener(PackageAddedListener listener){
        this._addedlisteners.add(listener);
    }

    public void addPackageremovedlistener(PackageRemovedListener listener){
        this._removedlisteners.add(listener);
    }
}
