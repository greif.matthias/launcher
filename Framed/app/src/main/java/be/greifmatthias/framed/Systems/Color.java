package be.greifmatthias.framed.Systems;

import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;

import java.util.ArrayList;
import java.util.List;

public class Color{
    public static int getDominantColor(Bitmap bitmap) {
        Palette palette = Palette.from(bitmap).maximumColorCount(1).generate();

        if(palette != null)
        {
            for(Palette.Swatch swatch:palette.getSwatches())
            {
                return swatch.getRgb();
            }
        }

        return android.graphics.Color.parseColor("#000000");
    }

    public static ArrayList<PaletteColor> getMaterial(){
        ArrayList<PaletteColor> colors = new ArrayList<PaletteColor>();

        String[] colorcodes = new String[]{
                "#F44336",
                "#E91E63",
                "#9C27B0",
                "#673AB7",
                "#3F51B5",
                "#2196F3",
                "#03A9F4",
                "#00BCD4",
                "#009688",
                "#4CAF50",
                "#8BC34A",
                "#CDDC39",
                "#FFEB3B",
                "#FFC107",
                "#FF9800",
                "#FF5722",
                "#795548",
                "#9E9E9E",
                "#607D8B",
                "#000000",
                "#FFFFFF"
        };

        for (String code:colorcodes) {
            colors.add(new PaletteColor(android.graphics.Color.parseColor(code)));
        }

        return colors;
    }

    public static int findClosestToPalette(final int color, final List<PaletteColor> colors) {
        int closestColor = -1;
        int closestDistance = Integer.MAX_VALUE;
        for (final PaletteColor paletteColor : colors) {
            final int distance = paletteColor.distanceTo(color);
            if (distance < closestDistance) {
                closestDistance = distance;
                closestColor = paletteColor.asInt();
            }
        }
        return closestColor;
    }

    public static final class PaletteColor {
        private final int r;
        private final int g;
        private final int b;
        private final int color;

        public PaletteColor(final int color) {
            this.r = ((color & 0xff000000) >>> 24);
            this.g = ((color & 0x00ff0000) >>> 16);
            this.b = ((color & 0x0000ff00) >>> 8);
            this.color = color;
        }

        public int distanceTo(final int color) {
            final int deltaR = this.r - ((color & 0xff000000) >>> 24);
            final int deltaG = this.g - ((color & 0x00ff0000) >>> 16);
            final int deltaB = this.b - ((color & 0x0000ff00) >>> 8);
            return (deltaR * deltaR) + (deltaG * deltaG) + (deltaB * deltaB);
        }

        public int asInt() {
            return this.color;
        }
    }
}
