package be.greifmatthias.framed.DialogViews;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import be.greifmatthias.framed.R;

public class DialogView {
    public void show(Activity activity, String msg){
        // Setup dialog
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialogview_root);

        // Setup UI
        TextView text = (TextView) dialog.findViewById(R.id.tvMessage);
        text.setText(msg);

        // Show
        dialog.show();
    }
}
